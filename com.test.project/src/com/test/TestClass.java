package com.test;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;


public class TestClass {
	 @Test(groups={"grp1"},priority=1)
	 public void testmethod(){
		 System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+File.separator+"resources"+File.separator+"chromedriver.exe");
		 WebDriver driver = new ChromeDriver();
		 driver.get("https://www.google.com");
		 System.out.println("testing done");
		 driver.close();
}
}
